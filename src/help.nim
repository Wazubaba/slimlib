
const LONG_HELP* = """USAGE: slimlib [OPTIONS] MODULE MOD_ARGUMENT_1..
OPTIONS:
  -h, --help  -  Show this help and exit.

MODULES:
  search  - search for a module matching the provided TAGS.
  install - install the TARGET to DEST. If DEST is a directory the TARGET will
            be copied into it. If DEST is a filename then TARGET will be copied
            as a file named DEST. If DEST exists then the user will be prompted
            on whether to overwrite or not. If DEST is the same name but is
            older than TARGET, it *will* be overwritten without prompt.
  list    - list all categories found.
  add     - add TARGET with tags TAGS. $EDITOR will be invoked to edit a file
            /tmp/desc which will be moved into the configured repository.
EXAMPLES:
  slimlib [OPTIONS] search TAGS
  slimlib [OPTIONS] install TARGET DEST
  slimlib [OPTIONS] list
  slimlib [OPTIONS] add TARGET TAG1 TAG2 TAG3..
"""

const SHORT_HELP* = "Try slimlib --help for more info"

