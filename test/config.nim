import json
import os
import strutils
import strformat
import parsetoml

type
  Format* = enum
    Toml,
    Json,
    Any

  JsonRule* = object
    name*: string
    kind*: JsonNodeKind

  TomlRule* = object
    name*: string
    kind*: TomlValueKind

  ConfigFormatError* = object of Exception
  ConfigValidationError* = object of Exception

proc detect_format*(path: string): Format {.raises: ConfigFormatError.} =
  ## Attempt to detect what kind of config we are dealing with
  case path.splitFile().ext.toLower():
    of ".toml": return Toml
    of ".json": return Json
    else:
      raise newException(ConfigFormatError, &"Cannot identify format for '{path}'")

#
# JSON UTILS
#
proc validate_json*(tree: JsonNode, rule: JsonRule)
  {.raises: ConfigValidationError.} =
  ## Validate a single rule within a json node
  if rule.name notin tree:
    raise newException(ConfigValidationError, &"Missing required key '{rule.name}' in config.")
  try:
    if tree[rule.name].kind != rule.kind:
      raise newException(ConfigValidationError, &"Key '{rule.name}' found, but wrong type. Expected '{$rule.kind}'.")

  except KeyError: discard # Cannot happen

proc validate_json*(tree: JsonNode, rules: seq[JsonRule])
  {.raises: ConfigValidationError.} =
  ## Validate a list of rules within a json node
  for rule in rules:
    validate_json(tree, rule)

proc validate_string_json*(tree: JsonNode, name: string): string
  {.raises: ConfigValidationError.} =
  ## Validate a key and return the value as string
  tree.validate_json(JsonRule(name: name, kind:JString))
  try: return tree[name].getStr()
  except KeyError: discard # Cannot happen

proc validate_int_json*(tree: JsonNode, name: string): int
  {.raises: ConfigValidationError.} =
  ## Validate a rule and return the value as int
  tree.validate_json(JsonRule(name:name, kind:JInt))
  try: return tree[name].getInt()
  except KeyError: discard # Cannot happen

proc validate_float_json*(tree: JsonNode, name: string): float
  {.raises: ConfigValidationError.} =
  ## Validate a rule and return the value as float
  tree.validate_json(JsonRule(name:name, kind:JFloat))
  try: return tree[name].getFloat()
  except KeyError: discard # Cannot happen

proc validate_bool_json*(tree: JsonNode, name: string): bool
  {.raises: ConfigValidationError.} =
  ## Validate a rule and return the value as bool
  tree.validate_json(JsonRule(name:name, kind:JBool))
  try: return tree[name].getBool()
  except KeyError: discard # Cannot happen

#
# TOML UTILS
#
proc validate_toml*(tree: TomlValueRef, rule: TomlRule)
  {.raises: ConfigValidationError.} =
  ## Validates a single rule within a toml node
  if rule.name notin tree:
    raise newException(ConfigValidationError, &"Missing required key '{rule.name}' in config.")
  try:
    if tree[rule.name].kind != rule.kind:
      raise newException(ConfigValidationError, &"Key '{rule.name}' found, but wrong type. Expected '{$rule.kind}'.")
  except KeyError: discard # Cannot happen

proc validate_toml*(tree: TomlValueRef, rules: seq[TomlRule])
  {.raises: ConfigValidationError.} =
  ## Validate a list of rules within a json node
  for rule in rules:
    validate_toml(tree, rule)

proc validate_string_toml*(tree: TomlValueRef, name: string): string
  {.raises: ConfigValidationError.} =
  ## Validate a key and return the value as string
  tree.validate_toml(TomlRule(name: name, kind: TomlValueKind.String))
  try: return tree[name].getStr()
  except KeyError: discard # Cannot happen

proc validate_int_toml*(tree: TomlValueRef, name: string): int
  {.raises: ConfigValidationError.} =
  ## Validate a rule and return the value as int
  tree.validate_toml(TomlRule(name:name, kind:TomlValueKind.Int))
  try: return tree[name].getInt()
  except KeyError: discard # Cannot happen

proc validate_float_toml*(tree: TomlValueRef, name: string): float
  {.raises: ConfigValidationError.} =
  ## Validate a rule and return the value as float
  tree.validate_toml(TomlRule(name:name, kind:TomlValueKind.Float))
  try: return tree[name].getFloat()
  except KeyError: discard # Cannot happen

proc validate_bool_toml*(tree: TomlValueRef, name: string): bool
  {.raises: ConfigValidationError.} =
  ## Validate a rule and return the value as bool
  tree.validate_toml(TomlRule(name:name, kind:TomlValueKind.Bool))
  try: return tree[name].getBool()
  except KeyError: discard # Cannot happen

