import terminal
import os
import strformat
import tables

type
  LogLevel* {.pure.} = enum
    all,
    info,
    warn,
    critical

  LogType* {.pure.} = enum
    message,
    info,
    warn,
    critical

  Logger* = ref object of RootObj
    name*: string
    minimumLevel*: LogLevel
    fileName: string
    output: File

proc set_file*(self: Logger, path: string) =
  ## Open a file that will be synchronized with log emissions. Performs a simple
  ## log rotation for up to 2 backups.
  if path.fileExists():
    if (path & ".1").fileExists():
      if (path & ".2").fileExists():
        removeFile(path & ".2")
      moveFile(path & ".1", path & ".2")
    moveFile(path, path & ".1")

  self.fileName = path
  self.output = open(self.fileName, fmAppend)

proc close_file*(self: Logger) =
  ## Close an open log file
  if self.fileName.len > 0:
    self.output.close()

proc log*(self: Logger, level: LogLevel, style: LogType, message: string) =
  ## Log a message. Offers more fine-grained control for implementing your own
  ## messages.
  if level >= self.minimumLevel:
    var color: ForegroundColor
    var spacer, header: string
    case style:
      of LogType.message: color = fgWhite; header = ""; spacer = ""
      of LogType.info: color = fgCyan; header = "info"; spacer = ": "
      of LogType.warn: color = fgYellow; header = "warn"; spacer = ": "
      of LogType.critical: color = fgRed; header = "critical"; spacer = ": "

    styledEcho(resetStyle, "[", styleBright, self.name, resetStyle, "] ",
      styleBright, color, header, resetStyle, spacer,  message)

    if self.fileName.len > 0:
      self.output.writeLine(fmt"[{self.name}] {header}{spacer}{message}")
      self.output.flushFile()

system.addQuitProc(resetAttributes)

proc message*(self: Logger, message: string) = self.log(LogLevel.all, LogType.message, message)
proc info*(self: Logger, message: string) = self.log(LogLevel.info, LogType.info, message)
proc warn*(self: Logger, message: string) = self.log(LogLevel.warn, LogType.warn, message)
proc critical*(self: Logger, message: string) = self.log(LogLevel.critical, LogType.critical, message)

